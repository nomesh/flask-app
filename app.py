from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql+psycopg2://postgres:123@127.0.0.1:5432/student"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Data(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    city = db.Column(db.String(50))
    contact = db.Column(db.String(50))

    def __init__(self, name, city, contact):
        self.name = name
        self.city = city
        self.contact = contact


@app.route('/')
def index():
    get_all_students = Data.query.all()
    return render_template('index.html', students=get_all_students)


@app.route('/addStudent', methods=['POST', 'GET'])
def add_student():
    name = request.form['name']
    city = request.form['city']
    contact = request.form['contact']
    mydata = Data(name, city, contact)
    db.session.add(mydata)
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/updateStudent', methods=['POST', 'GET'])
def update_student():
    if request.method == 'POST':
        mydata = Data.query.get(request.form.get('id'))
        mydata.name = request.form['name']
        mydata.city = request.form['city']
        mydata.contact = request.form['contact']
        db.session.commit()
        return redirect(url_for('index'))


# Delete student
@app.route('/deleteStudent/<id>', methods=['POST', 'GET'])
def delete_student(id):
    mydata = Data.query.get(id)
    db.session.delete(mydata)
    db.session.commit()
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)
